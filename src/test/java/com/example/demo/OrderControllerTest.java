package com.example.demo;


import com.example.demo.controllers.OrderController;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.UserOrder;

import com.example.demo.model.persistence.repositories.OrderRepository;
import com.example.demo.model.persistence.repositories.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderControllerTest {
    private OrderController orderController;
    private UserRepository userRepo = mock(UserRepository.class);
    private OrderRepository orderRepo = mock(OrderRepository.class);


    @Before
    public void setUp()
    {
        orderController = new OrderController();
        TestUtils.initObjects(orderController,"userRepository",userRepo);
        TestUtils.initObjects(orderController,"orderRepository",orderRepo);
    }

    @Test
    public void create_order_happy_path()
    {
        Item item = new Item();
        item.setPrice(BigDecimal.valueOf(25.5));
        item.setName("Gofret");
        item.setDescription("Ulker Cikolatali Gofret");
        item.setId((long)1);
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        user.setId(1);
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        cart.setItems(Arrays.asList(item));
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);


        ResponseEntity<UserOrder> responseEntity = orderController.submit("koray");
        assertNotNull(responseEntity);
        assertEquals(200,responseEntity.getStatusCodeValue());

        UserOrder userOrder = responseEntity.getBody();
        assertNotNull(userOrder);
        assertEquals(user, userOrder.getUser());
        assertEquals(BigDecimal.valueOf(150.53),userOrder.getTotal());

        when(orderRepo.findByUser(user)).thenReturn(Arrays.asList(userOrder));

        ResponseEntity<List<UserOrder>> getOrderResponseEntity = orderController.getOrdersForUser("koray");
        assertNotNull(getOrderResponseEntity);
        assertEquals(200,getOrderResponseEntity.getStatusCodeValue());

        UserOrder getOrderUserOrder = getOrderResponseEntity.getBody().get(0);

        assertNotNull(getOrderUserOrder);
        assertEquals(user, getOrderUserOrder.getUser());
        assertEquals(BigDecimal.valueOf(150.53),getOrderUserOrder.getTotal());
        assertEquals(item.getDescription(),getOrderUserOrder.getItems().get(0).getDescription());
        assertEquals(item.getId(),getOrderUserOrder.getItems().get(0).getId());
        assertEquals(item.getName(),getOrderUserOrder.getItems().get(0).getName());
        assertEquals(item.getPrice(),getOrderUserOrder.getItems().get(0).getPrice());
    }


    @Test
    public void create_order_negative_path()
    {
        Item item = new Item();
        item.setPrice(BigDecimal.valueOf(25.5));
        item.setName("Gofret");
        item.setDescription("Ulker Cikolatali Gofret");
        item.setId((long)1);
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        user.setId(1);
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        cart.setItems(Arrays.asList(item));
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);


        ResponseEntity<UserOrder> responseEntity = orderController.submit("koray");
        assertNotNull(responseEntity);
        assertEquals(200,responseEntity.getStatusCodeValue());

        UserOrder userOrder = responseEntity.getBody();
        assertNotNull(userOrder);
        assertEquals(user, userOrder.getUser());
        assertEquals(BigDecimal.valueOf(150.53),userOrder.getTotal());

        when(orderRepo.findByUser(user)).thenReturn(Arrays.asList(userOrder));

        ResponseEntity<List<UserOrder>> getOrderResponseEntity = orderController.getOrdersForUser("ko1ray");
        assertNotNull(getOrderResponseEntity);
        assertEquals(404,getOrderResponseEntity.getStatusCodeValue());
    }
}
