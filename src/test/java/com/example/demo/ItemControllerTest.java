package com.example.demo;

import com.example.demo.controllers.CartController;
import com.example.demo.controllers.ItemController;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.requests.ModifyCartRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ItemControllerTest {

    private ItemController itemController;
    private ItemRepository itemRepo = mock(ItemRepository.class);

    @Before
    public void setUp()
    {
        itemController = new ItemController();
        TestUtils.initObjects(itemController,"itemRepository",itemRepo);
    }

    @Test
    public void create_item_happy_path()
    {
        Item item = new Item();
        item.setId((long)1);
        item.setDescription("Ülker Çikolatalı Gofret");
        item.setName("Gofret");
        item.setPrice(BigDecimal.valueOf(25.53));
        when(itemRepo.findAll()).thenReturn(Arrays.asList(item));
        when(itemRepo.findById((long)1)).thenReturn(Optional.of(item));
        when(itemRepo.findByName("gofret")).thenReturn(Arrays.asList(item));

        ResponseEntity<List<Item>> getItemsResponseEntity = itemController.getItems();
        assertNotNull(getItemsResponseEntity);
        assertEquals(200,getItemsResponseEntity.getStatusCodeValue());

        Item obtainedItem = getItemsResponseEntity.getBody().get(0);
        assertNotNull(obtainedItem);
        assertEquals(Optional.of((long)1).get(), obtainedItem.getId());
        assertEquals(BigDecimal.valueOf(25.53),obtainedItem.getPrice());
        assertEquals("Ülker Çikolatalı Gofret",obtainedItem.getDescription());
        assertEquals("Gofret",obtainedItem.getName());

        ResponseEntity<Item> getItemByIdResponseEntity = itemController.getItemById((long)1);
        assertNotNull(getItemByIdResponseEntity);
        assertEquals(200,getItemByIdResponseEntity.getStatusCodeValue());

        Item getItemByIdItem = getItemByIdResponseEntity.getBody();
        assertNotNull(getItemByIdItem);
        assertEquals(Optional.of((long)1).get(), getItemByIdItem.getId());
        assertEquals(BigDecimal.valueOf(25.53),getItemByIdItem.getPrice());
        assertEquals("Ülker Çikolatalı Gofret",getItemByIdItem.getDescription());
        assertEquals("Gofret",getItemByIdItem.getName());


        ResponseEntity<List<Item>> getItemsByNameResponseEntity = itemController.getItemsByName("gofret");
        assertNotNull(getItemsByNameResponseEntity);
        assertEquals(200,getItemsByNameResponseEntity.getStatusCodeValue());

        Item getItemsByNameItem = getItemsByNameResponseEntity.getBody().get(0);
        assertNotNull(getItemsByNameItem);
        assertEquals(Optional.of((long)1).get(), getItemsByNameItem.getId());
        assertEquals(BigDecimal.valueOf(25.53),getItemsByNameItem.getPrice());
        assertEquals("Ülker Çikolatalı Gofret",getItemsByNameItem.getDescription());
        assertEquals("Gofret",getItemsByNameItem.getName());
    }

    @Test
    public void create_item_negative_case()
    {
        Item item = new Item();
        item.setId((long)1);
        item.setDescription("Ülker Çikolatalı Gofret");
        item.setName("Gofret");
        item.setPrice(BigDecimal.valueOf(25.53));
        when(itemRepo.findAll()).thenReturn(Arrays.asList(item));
        when(itemRepo.findById((long)1)).thenReturn(Optional.of(item));
        when(itemRepo.findByName("gofret")).thenReturn(Arrays.asList(item));

        ResponseEntity<Item> getItemByIdResponseEntity = itemController.getItemById((long)0);
        assertNotNull(getItemByIdResponseEntity);
        assertEquals(404,getItemByIdResponseEntity.getStatusCodeValue());


        ResponseEntity<List<Item>> getItemsByNameResponseEntity = itemController.getItemsByName("gof1ret");
        assertNotNull(getItemsByNameResponseEntity);
        assertEquals(404,getItemsByNameResponseEntity.getStatusCodeValue());

    }
}
