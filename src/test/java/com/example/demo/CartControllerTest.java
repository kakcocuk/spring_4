package com.example.demo;

import com.example.demo.controllers.CartController;
import com.example.demo.controllers.UserController;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import com.example.demo.model.requests.ModifyCartRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CartControllerTest {
        private CartController cartController;
        private UserRepository userRepo = mock(UserRepository.class);
        private CartRepository cartRepo = mock(CartRepository.class);
        private ItemRepository itemRepo = mock(ItemRepository.class);

        @Before
        public void setUp()
        {
            cartController = new CartController();
            TestUtils.initObjects(cartController,"userRepository",userRepo);
            TestUtils.initObjects(cartController,"cartRepository",cartRepo);
            TestUtils.initObjects(cartController,"itemRepository",itemRepo);
        }
    @Test
    public void create_cart_happy_path()
    {
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);
        Item item = new Item();
        item.setId((long)1);
        item.setDescription("chocolatte");
        item.setName("ulker");
        item.setPrice(BigDecimal.valueOf(25.55));
        when(itemRepo.findById((long)1)).thenReturn(Optional.of(item));
        ModifyCartRequest modifyCartRequest = new ModifyCartRequest();
        modifyCartRequest.setItemId(1);
        modifyCartRequest.setQuantity(5);
        modifyCartRequest.setUsername("koray");

        ResponseEntity<Cart> cartResponseEntity = cartController.addTocart(modifyCartRequest);
        assertNotNull(cartResponseEntity);
        assertEquals(200,cartResponseEntity.getStatusCodeValue());

        Cart cartResponse = cartResponseEntity.getBody();
        assertNotNull(cartResponse);
        assertEquals(Optional.of((long)1).get(), cartResponse.getId());
        assertEquals(BigDecimal.valueOf(278.28),cartResponse.getTotal());
        assertEquals(0,cartResponse.getUser().getId());


        ResponseEntity<Cart> removeFromcartEntity = cartController.removeFromcart(modifyCartRequest);
        assertNotNull(removeFromcartEntity);
        assertEquals(200,removeFromcartEntity.getStatusCodeValue());

        Cart removeFromcartEntityBody = removeFromcartEntity.getBody();
        assertNotNull(removeFromcartEntityBody);
        assertEquals(Optional.of((long)1).get(), removeFromcartEntityBody.getId());
        assertEquals(BigDecimal.valueOf(150.53),removeFromcartEntityBody.getTotal());
        assertEquals(0,removeFromcartEntityBody.getUser().getId());
    }

    @Test
    public void get_car_negative()
    {
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);
        Item item = new Item();
        item.setId((long)1);
        item.setDescription("chocolatte");
        item.setName("ulker");
        item.setPrice(BigDecimal.valueOf(25.55));
        when(itemRepo.findById((long)1)).thenReturn(Optional.of(item));
        ModifyCartRequest modifyCartRequest = new ModifyCartRequest();
        modifyCartRequest.setItemId(1);
        modifyCartRequest.setQuantity(5);
        modifyCartRequest.setUsername("koray");

        ResponseEntity<Cart> cartResponseEntity = cartController.addTocart(modifyCartRequest);
        assertNotNull(cartResponseEntity);
        assertEquals(200,cartResponseEntity.getStatusCodeValue());

        Cart cartResponse = cartResponseEntity.getBody();
        assertNotNull(cartResponse);
        assertEquals(Optional.of((long)1).get(), cartResponse.getId());
        assertEquals(BigDecimal.valueOf(278.28),cartResponse.getTotal());
        assertEquals(0,cartResponse.getUser().getId());

        modifyCartRequest.setItemId(2);
        ResponseEntity<Cart> removeFromcartEntity = cartController.removeFromcart(modifyCartRequest);
        assertNotNull(removeFromcartEntity);
        assertEquals(404,removeFromcartEntity.getStatusCodeValue());

        modifyCartRequest.setItemId(1);
        modifyCartRequest.setUsername("kora1y");
        ResponseEntity<Cart> removeFromcartEntity1 = cartController.removeFromcart(modifyCartRequest);
        assertNotNull(removeFromcartEntity1);
        assertEquals(404,removeFromcartEntity1.getStatusCodeValue());

    }
}
