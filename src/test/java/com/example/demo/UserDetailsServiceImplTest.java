package com.example.demo;


import com.example.demo.controllers.CartController;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.ModifyCartRequest;
import com.example.demo.security.UserDetailsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserDetailsServiceImplTest {

    private UserDetailsServiceImpl userDetailsService;
    private UserRepository userRepo = mock(UserRepository.class);


    @Before
    public void setUp()
    {
        userDetailsService = new UserDetailsServiceImpl(userRepo);
        TestUtils.initObjects(userDetailsService,"userRepository",userRepo);
    }

    @Test
    public void create_userDetail_happy_path()
    {
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);

        UserDetails userDetails = userDetailsService.loadUserByUsername("koray");

        assertNotNull(userDetails);
        assertEquals("koray", userDetails.getUsername());
        assertEquals("akcocuk", userDetails.getPassword());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void create_userDetail_negative_path()
    {
        User user = new User();
        user.setUsername("koray");
        user.setPassword("akcocuk");
        Cart cart = new Cart();
        cart.setId((long)1);
        cart.setTotal(BigDecimal.valueOf(150.53));
        cart.setUser(user);
        user.setCart(cart);
        when(userRepo.findByUsername("koray")).thenReturn(user);

        UserDetails userDetails = userDetailsService.loadUserByUsername("k1oray");

    }
}
