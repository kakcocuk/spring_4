package com.example.demo;

import com.example.demo.controllers.UserController;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserControllerTest {
    private UserController userController;
    private UserRepository userRepo = mock(UserRepository.class);
    private CartRepository cartRepo = mock(CartRepository.class);
    private BCryptPasswordEncoder encoder = mock(BCryptPasswordEncoder.class);

@Before
public void setUp()
{
    userController = new UserController();
    TestUtils.initObjects(userController,"userRepository",userRepo);
    TestUtils.initObjects(userController,"cartRepository",cartRepo);
    TestUtils.initObjects(userController,"bCryptPasswordEncoder",encoder);

}


    @Test
    public void create_user_happy_path()
    {
        when(encoder.encode("akcocuk")).thenReturn("thisIsHashed");
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername("koray");
        createUserRequest.setPassword("akcocuk");
        createUserRequest.setConfirmedPassword("akcocuk");

        final ResponseEntity<User> responseEntity = userController.createUser(createUserRequest);
        assertNotNull(responseEntity);
        assertEquals(200,responseEntity.getStatusCodeValue());

        User user = responseEntity.getBody();
        assertNotNull(user);
        assertEquals("koray",user.getUsername());
        assertEquals("thisIsHashed",user.getPassword());
        assertEquals(0,user.getId());


        when(userRepo.findById((long)0)).thenReturn(Optional.of(user));
        final ResponseEntity<User> findByIdEntity = userController.findById((long)0);
        assertNotNull(findByIdEntity);
        assertEquals(200,findByIdEntity.getStatusCodeValue());

        User findUser = findByIdEntity.getBody();
        assertNotNull(findUser);
        assertEquals("koray",findUser.getUsername());
        assertEquals("thisIsHashed",findUser.getPassword());
        assertEquals(0,findUser.getId());

        when(userRepo.findByUsername("koray")).thenReturn(user);
        final ResponseEntity<User> findByUserNameEntity = userController.findByUserName("koray");
        assertNotNull(findByUserNameEntity);
        assertEquals(200,findByUserNameEntity.getStatusCodeValue());

        User findUsernameUser = findByUserNameEntity.getBody();
        assertNotNull(findUsernameUser);
        assertEquals("koray",findUsernameUser.getUsername());
        assertEquals("thisIsHashed",findUsernameUser.getPassword());
        assertEquals(0,findUser.getId());

    }

    @Test
    public void find_user_negative_case()
    {
        when(encoder.encode("akcocuk")).thenReturn("thisIsHashed");
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername("koray");
        createUserRequest.setPassword("akcocuk");
        createUserRequest.setConfirmedPassword("akcocuk");

        final ResponseEntity<User> responseEntity = userController.createUser(createUserRequest);
        assertNotNull(responseEntity);
        assertEquals(200,responseEntity.getStatusCodeValue());

        User user = responseEntity.getBody();
        assertNotNull(user);
        assertEquals("koray",user.getUsername());
        assertEquals("thisIsHashed",user.getPassword());
        assertEquals(0,user.getId());


        when(userRepo.findById((long)0)).thenReturn(Optional.of(user));
        final ResponseEntity<User> findByIdEntity = userController.findById((long)1);
        assertNotNull(findByIdEntity);
        assertEquals(404,findByIdEntity.getStatusCodeValue());


        when(userRepo.findByUsername("koray")).thenReturn(user);
        final ResponseEntity<User> findByUserNameEntity = userController.findByUserName("koray1");
        assertNotNull(findByUserNameEntity);
        assertEquals(404,findByUserNameEntity.getStatusCodeValue());


    }

}
